/** Guarentee of a non-empty string */
export declare type noemp = Exclude<string, ""> | null;
/** Guarentee of a non-empty string */
export declare const noemp: ((s: any) => noemp);
/** Moves the this parameter out of the called function */
export declare const fbind: <F extends (this: T, args: any[]) => R, T, R>(fn: F) => (self: T, ...args: Parameters<F>) => R;
/** Map an iterable list, filtering as necessary */
export declare function filter_map<T, U>(list: T[] | IterableIterator<T>, f: ((t: T) => U | null | undefined)): IterableIterator<U>;
/** Mark a performance index */
export declare const mark: (markName: string) => void;
/** Measure a named performance index */
export declare const measure: (name: string) => void;
/** Measure a function call period */
export declare const measure_fn: <U extends (...args: any[]) => V, V>(name: string, fn: U, ...args: Parameters<U>) => V;
/** Measure a wrapped function call duration */
export declare const wrap_measure: <U extends (...args: any[]) => V, V>(name: string, fn: U) => U;
/** Measure an async function call-return period */
export declare const measure_fn_async: <U extends (...args: any[]) => Promise<V>, V>(name: string, fn: U, ...args: Parameters<U>) => Promise<V>;
/** Measure a wrapped async function call-return period */
export declare const wrap_measure_async: <U extends (...args: any[]) => Promise<V>, V>(name: string, fn: U) => U;
export declare type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;
/** Wait for decode image */
export declare const decode: ((img: HTMLImageElement) => Promise<void>);
declare type Appendable = HTMLElement | DocumentFragment | string;
declare type Queryable = Document | DocumentFragment | Element;
export declare type MaybeOr<T> = T | false | 0 | null | '' | undefined;
export declare type IterableOrNot<T> = MaybeOr<Iterable<MaybeOr<T>>>;
interface closest {
    <T extends keyof HTMLElementTagNameMap, K extends Element>(sels: T, ctx: K): HTMLElementTagNameMap[T];
    <T extends keyof SVGElementTagNameMap, K extends Element>(sels: T, ctx: K): SVGElementTagNameMap[T];
    <T extends Element>(sels: string, ctx: Element, tp?: new () => T): T;
}
interface queryselector {
    <T extends keyof HTMLElementTagNameMap>(sels: T, ctx?: Queryable): HTMLElementTagNameMap[T];
    <T extends keyof SVGElementTagNameMap>(sels: T, ctx?: Queryable): SVGElementTagNameMap[T];
    <T extends Element>(sels: string, ctx: Queryable, tp?: new () => T): T;
}
interface queryselectorall {
    <T extends keyof HTMLElementTagNameMap>(sels: T, ctx?: Queryable): HTMLElementTagNameMap[T][];
    <T extends keyof SVGElementTagNameMap>(sels: T, ctx?: Queryable): SVGElementTagNameMap[T][];
    <T extends Element>(sels: string, ctx: Queryable, tp?: new () => T): T[];
}
interface createelement<E> {
    <T extends keyof E>(el: T, o: Partial<Omit<E[T], "children" | "childNodes" | "classList" | "style">>): E[T];
}
export declare const enum DOCERR {
    NOT_FOUND = 0,
    BAD_TYPE = 1
}
/** Create a document fragment container */
export declare const $f: () => DocumentFragment;
/** Remove an element */
export declare const $rm: (el: Element) => void;
/** Select the closest ancestor */
export declare const $closest: closest;
/** Select at most 1 in a given context */
export declare const $: queryselector;
/** Select all matching in a given context */
export declare const $$: queryselectorall;
/** Create element (with attributes) */
export declare const $ce: createelement<HTMLElementTagNameMap>;
/** Create element (with children, attributes) */
export declare const $cec: <T extends "object" | "mark" | "a" | "abbr" | "address" | "applet" | "area" | "article" | "aside" | "audio" | "b" | "base" | "basefont" | "bdi" | "bdo" | "blockquote" | "body" | "br" | "button" | "canvas" | "caption" | "cite" | "code" | "col" | "colgroup" | "data" | "datalist" | "dd" | "del" | "details" | "dfn" | "dialog" | "dir" | "div" | "dl" | "dt" | "em" | "embed" | "fieldset" | "figcaption" | "figure" | "font" | "footer" | "form" | "frame" | "frameset" | "h1" | "h2" | "h3" | "h4" | "h5" | "h6" | "head" | "header" | "hgroup" | "hr" | "html" | "i" | "iframe" | "img" | "input" | "ins" | "kbd" | "label" | "legend" | "li" | "link" | "main" | "map" | "marquee" | "menu" | "meta" | "meter" | "nav" | "noscript" | "ol" | "optgroup" | "option" | "output" | "p" | "param" | "picture" | "pre" | "progress" | "q" | "rp" | "rt" | "ruby" | "s" | "samp" | "script" | "section" | "select" | "slot" | "small" | "source" | "span" | "strong" | "style" | "sub" | "summary" | "sup" | "table" | "tbody" | "td" | "template" | "textarea" | "tfoot" | "th" | "thead" | "time" | "title" | "tr" | "track" | "u" | "ul" | "var" | "video" | "wbr">(name: T, children?: MaybeOr<Iterable<MaybeOr<Appendable>>>, attrs?: Pick<Partial<HTMLElementTagNameMap[T]>, Exclude<keyof HTMLElementTagNameMap[T], "children" | "childNodes" | "innerText" | "outerHTML" | "innerHTML">> | undefined) => HTMLElementTagNameMap[T];
/** Create container (with classlist) */
export declare const $cc: (n: string, c?: MaybeOr<Iterable<MaybeOr<Appendable>>>) => HTMLDivElement;
/** Create span (with classlist) */
export declare const $cs: (n: string, c?: MaybeOr<Iterable<MaybeOr<Appendable>>>) => HTMLSpanElement;
export {};
//# sourceMappingURL=index.d.ts.map