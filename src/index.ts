/** Guarentee of a non-empty string */
export type noemp = Exclude<string, ""> | null;
/** Guarentee of a non-empty string */
export const noemp: ((s: any) => noemp) = s => s && (""+s)["trim"]() || null;
/** Moves the this parameter out of the called function */
//@ts-ignore
export const fbind = <F extends (this: T, args: any[]) => R, T, R>(fn: F): ((self: T, ...args: Parameters<F>) => R) => Function["prototype"]["call"]["bind"](fn)

/** Map an iterable list, filtering as necessary */
export function* filter_map<T, U>(list: T[] | IterableIterator<T>, f: ((t: T) => U | null | undefined)): IterableIterator<U> {
    let v, k
    for (k of list) if ((v = f(k)) != null) yield v;
}

/** Mark a performance index */
export const mark = performance["mark"]["bind"](performance);
/** Measure a named performance index */
export const measure = (name: string) => performance["measure"](name, name)
/** Measure a function call period */
export const measure_fn = <U extends (...args: any[]) => V, V>(name: string, fn: U, ...args: Parameters<U>): V => {
    try {
        mark(name);
        return fn(...args);
    } finally {
        measure(name);
    }
}
/** Measure a wrapped function call duration */
//@ts-ignore
export const wrap_measure = <U extends (...args: any[]) => V, V>(name: string, fn: U): U => (...args: Parameters<U>): V => measure_fn(name, fn, ...args);

/** Measure an async function call-return period */
//@ts-ignore
export const measure_fn_async = async <U extends (...args: any[]) => Promise<V>, V>(name: string, fn: U, ...args: Parameters<U>): Promise<V> => {
    try {
        mark(name);
        return await fn(...args);
    } finally {
        measure(name);
    }
}
/** Measure a wrapped async function call-return period */
//@ts-ignore
export const wrap_measure_async = <U extends (...args: any[]) => Promise<V>, V>(name: string, fn: U): U => (...args: Parameters<U>): Promise<V> => measure_fn_async(name, fn, ...args);

export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;

/** Wait for decode image */
//@ts-ignore
export const decode: ((img: HTMLImageElement) => Promise<void>) = wrap_measure_async<(arg: undefined) => void, never, void>('img-decode', HTMLImageElement["prototype"]["decode"]
? fbind(HTMLImageElement["prototype"]["decode"]) as ((img: HTMLImageElement) => Promise<void>)
: (img: HTMLImageElement) => img["complete"] ? Promise["resolve"]() : new Promise<void>((r, j) => {
    const rel = () => (
        img["removeEventListener"]("error", rej),
        img["removeEventListener"]("load", res)
    ),
        res = (): void => (img["complete"] ? r() : requestAnimationFrame(() => r()), rel()),
        rej: ((e: ErrorEvent) => void) = e => (j(e), rel());
    img["addEventListener"]("error", rej);
    img["addEventListener"]("load", res);
}));

type Appendable = HTMLElement | DocumentFragment | string;
type Queryable = Document | DocumentFragment | Element;
export type MaybeOr<T> = T | false | 0 | null | '' | undefined;
export type IterableOrNot<T> = MaybeOr<Iterable<MaybeOr<T>>>;

interface closest {
    <T extends keyof HTMLElementTagNameMap, K extends Element>(
        sels: T,
        ctx: K
    ): HTMLElementTagNameMap[T];
    <T extends keyof SVGElementTagNameMap, K extends Element>(
        sels: T,
        ctx: K
    ): SVGElementTagNameMap[T];
    <T extends Element>(
        sels: string,
        ctx: Element,
        tp?: new() => T
    ): T;
}
interface queryselector {
    <T extends keyof HTMLElementTagNameMap>(
        sels: T,
        ctx?: Queryable
    ): HTMLElementTagNameMap[T];
    <T extends keyof SVGElementTagNameMap>(
        sels: T,
        ctx?: Queryable
    ): SVGElementTagNameMap[T];
    <T extends Element>(
        sels: string,
        ctx: Queryable,
        tp?: new() => T
    ): T;
}
interface queryselectorall {
    <T extends keyof HTMLElementTagNameMap>(
        sels: T,
        ctx?: Queryable
    ): HTMLElementTagNameMap[T][];
    <T extends keyof SVGElementTagNameMap>(
        sels: T,
        ctx?: Queryable
    ): SVGElementTagNameMap[T][];
    <T extends Element>(
        sels: string,
        ctx: Queryable,
        tp?: new() => T
    ): T[];
}
interface createelement<E> {
    <T extends keyof E>(el: T, o: Partial<Omit<E[T], "children" | "childNodes" | "classList" | "style">>): E[T];
}
export const enum DOCERR { NOT_FOUND, BAD_TYPE }

/** Create a document fragment container */
export const $f = () => document["createDocumentFragment"]();
/** Remove an element */
export const $rm = (el: Element) => { el["remove"](); };
/** Select the closest ancestor */
export const $closest: closest = <T>(sels: string, ctx: Element, tp?: new() => T): T => {
    let cls = tp || HTMLElement,
    el = ctx["closest"](sels);
    if (el instanceof cls) return el as T;
    throw el ? DOCERR.NOT_FOUND : DOCERR.BAD_TYPE;
};
/** Select at most 1 in a given context */
export const $: queryselector = <T>(sels: string, ctx: Queryable = document, tp?: new() => T): T => {
    let cls = tp || HTMLElement,
    el = ctx["querySelector"](sels);
    if (el instanceof cls) return el as T;
    throw el ? DOCERR.NOT_FOUND : DOCERR.BAD_TYPE;
};
/** Select all matching in a given context */
export const $$: queryselectorall = <T>(sels: string, ctx: Queryable = document, tp?: new() => T): T[] => {
    let cls = tp || HTMLElement,
    els = ctx["querySelectorAll"](sels),
    i = 0,
    j = 0,
    l = els["length"],
    rt: T[] = Array(l);
    do els[i] instanceof cls && (rt[j++] = els[i] as unknown as T);
    while (++i < l)
    rt["length"] = j;
    return rt;
};
/** Create element (with attributes) */
export const $ce: createelement<HTMLElementTagNameMap> = (el, o) => Object["assign"](document["createElement"](el), o);
/** Create element (with children, attributes) */
export const $cec = <T extends keyof HTMLElementTagNameMap>(
    name: T,
    children?: IterableOrNot<Appendable>,
    attrs?: Omit<Partial<HTMLElementTagNameMap[T]>, "children" | "childNodes" | "innerHTML" | "innerText" | "outerHTML">,
) => {
    let container = $ce(name, (attrs || {}) as unknown as any), el: MaybeOr<Appendable>;
    if (children) for (el of children) el && container["append"](el);
    return container
};
/** Create container (with classlist) */
export const $cc = (n: string, c?: IterableOrNot<Appendable>) => $cec("div", c, {"className": n});
/** Create span (with classlist) */
export const $cs = (n: string, c?: IterableOrNot<Appendable>) => $cec("span", c, {"className": n});
